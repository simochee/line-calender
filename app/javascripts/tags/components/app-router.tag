<app-router>

  <router>
    <route path="/"><home /></route>
  </router>

  <script>
    import 'riot-route/lib/tag'

    import '../containers/home'
  </script>
</app-router>
